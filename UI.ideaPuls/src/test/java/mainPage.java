import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import common.rand;
import io.qameta.allure.Step;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

public class mainPage extends baseTest{
    StartPage startPage = new StartPage();
    rand randomData = new rand();

    @Listeners
    public class Tests {


        @Test
        @Step ("Проверка заголовка")
        public void testLoginTitle() {
            String expectedTitle = "Пульс-страхование";
            String actualTitle = Selenide.title();
            Assert.assertEquals(actualTitle, expectedTitle);
        }

        @Test
        @Step ("Проверка надписи на странице")
        public void testTextMain() {
            startPage.headMain.shouldHave(Condition.text("Вы уже регистрировались на нашем сайте или в приложении?"));
        }

        @Test
        public void login() {
            startPage.buttonY.shouldBe(Condition.visible);
            startPage.buttonY.click();
            startPage.next.click();
            startPage.insuranceTitle.shouldBe(Condition.visible);
            startPage.insuranceHouse.click();
            startPage.nextToPhone.click();
            startPage.phoneNumber.shouldBe(Condition.visible);
        }

        @Test
        public void phoneNumberVisible() {
            String randomPhone = randomData.generatePhoneNumber();
            startPage.buttonY.shouldBe(Condition.visible);
            startPage.buttonY.click();
            startPage.next.click();
            startPage.insuranceTitle.shouldBe(Condition.visible);
            startPage.insuranceHouse.click();
            startPage.nextToPhone.click();
            startPage.phoneNumber.shouldBe(Condition.visible);
            startPage.inputPhone.setValue(randomPhone);
            startPage.getCode.click();
            startPage.phoneText.shouldBe(Condition.text("Мы отправили SMS на номер +7 " + randomPhone));

        }
    }
}
