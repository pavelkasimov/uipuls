import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class StartPage {
    SelenideElement buttonY = $(byXpath("//*[@class='sc-hjQCSK bXzMdp' and text()='Да']"));
    SelenideElement next = $(byXpath("//span[@class='sc-csvncw ffqHrL' and text()='Продолжить']"));
    SelenideElement insuranceTitle = $(byXpath("//h2[@class='sc-igHpSv bTeRxA']"));
    SelenideElement insuranceHouse = $(byXpath("//div[@class='sc-jhzXDd bvSbyF']"));
    SelenideElement nextToPhone = $(byXpath("//div[@class='sc-grREDI htjJZw']"));
    SelenideElement phoneNumber = $(byXpath("//h1[@class='sc-bTmccw fUguTn']"));
    SelenideElement headMain = $(byXpath("//h3[@class='sc-hKdnnL gDOlBS']"));
    SelenideElement inputPhone = $(byXpath("//input[@class='sc-lkwKjF bcetiI']"));
    SelenideElement getCode = $(byXpath("//div[@class='sc-grREDI htjJZw sc-liHMlC bmMwfZ']"));
    SelenideElement phoneText = $(byXpath("//h3[@class='sc-hQRsPl gTBsjA']"));
}
