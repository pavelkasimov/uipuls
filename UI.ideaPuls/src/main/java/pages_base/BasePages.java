package pages_base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Selenide.open;
import static constants.Constant.TimeOutVariable.ELEMENT_WAIT;

public class BasePages {
    protected WebDriver driver;

    public BasePages(WebDriver driver) {
        this.driver = driver;
    }
    public void baseUrl (){open("https://lk.pulse.insure/");
        driver.get("https://lk.pulse.insure/");
    }
    public WebElement waitElementIsVisible (WebElement element){
    new WebDriverWait(driver, ELEMENT_WAIT).until(ExpectedConditions.visibilityOf(element));
    return element;
    }
}
