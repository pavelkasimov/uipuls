package constants;

import java.time.Duration;

public class Constant {
    public static class TimeOutVariable {
        public static final int IMPLICIT_WAIT = 4;
        public static final Duration ELEMENT_WAIT = Duration.ofSeconds(8);
    }
}